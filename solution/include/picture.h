#ifndef POPITKA_2_PICTURE_H
#define POPITKA_2_PICTURE_H
#include <stdint.h>
struct pixel { uint8_t b, g, r; };

struct fillImage {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};
void makeNewImage(struct fillImage* , uint64_t , uint64_t);
struct fillImage imageRotate(struct fillImage);
void imageFree(struct fillImage* img);
#endif //POPITKA_2_PICTURE_H

